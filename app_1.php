<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <body>
        <div ng-app="myApp" ng-controller="MyCtrl">
            First Name : <input type="text" ng-model="FirstName"><br/>
            Last  Name : <input type="text" ng-model="LastName"><br/>
            <br/>
            Full Name : {{FirstName + " "+ LastName}} 
        </div>
        
        <script>
            var apps = angular.module('myApp',[]);
            apps.controller('MyCtrl',function($scope){
                $scope.FirstName = "Rajesh";
                $scope.LastName = "Palande";
            });
        </script>
    </body>
</html>