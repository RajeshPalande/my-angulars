<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <body>
        <div ng-app="myApp" ng-controller="myController">
            First NAME : <input ng-model="FirstName" />
            <br/>
            Last NAME : <input ng-model="LastName" />
            <br/>
            Full Name : {{ FirstName + " " + LastName }}
        </div>
        
        <script>
            var app = angular.module('myApp',[]);
            app.controller('myController',function($scope){
                $scope.FirstName = "Rajesh";
                $scope.LastName = "Palande";
            });
        </script>
    </body>
</html>