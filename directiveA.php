<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    <!--Attribute-->
    <body ng-app="myApp">
        <my-directive></my-directive>
        
        <div my-directive></div><!--Attribute-->
        
        <script>
            var app = angular.module("myApp",[]);
            app.directive("myDirective",function(){
                return{
                    restrict : "A",
                    template : "It's Now Work"
                };
            });
        </script>
    </body>
</html>