<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    <!--Class-->
    <body ng-app="myApp">
        <div class="my-directive"></div>
        
        <script>
            var app = angular.module("myApp",[]);
            app.directive("myDirective",function(){
                return{
                    restrict : "C",
                    template : "In CLass Working"
                };
            });
        </script>
    </body>
</html>