<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    <!--Element-->
    <body ng-app="myApp">
        <my-directive></my-directive>
        
        <script>
            var app = angular.module("myApp",[]);
            app.directive("myDirective",function(){
                return{
                   template:"It's Working" 
                };
            });
        </script>
    </body>
</html>