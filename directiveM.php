<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    <!--Comment(We've added the replace property in this example, otherwise the comment would be invisible.)-->
    <body ng-app="myApp">
        <!-- directive: my-directive -->
        <script>
            var app = angular.module("myApp", []);
            app.directive("myDirective",function(){
                return{
                    restrict : "M",
                    replace : true,
                    template : "<p>Commented Code See</p>"
                };
            });
        </script>
    </body>
</html>