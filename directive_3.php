<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <!--Create New Directives start-->
    <body ng-app="myApp">
        <my-directive></my-directive>
        <script>
            var app = angular.module("myApp",[]);
            app.directive("myDirective",function(){
                return{
                    template : "Hi, Now Working"
                };
            });
        </script>
    </body>
    <!--Create New Directives end-->
</html>