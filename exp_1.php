<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <body>
        <!--AngularJS Expressions start-->
<!--        <div ng-app="">
             New Add {{ 5 + 5 }}
        </div>-->
<!--<div ng-app="">
    <span ng-bind="5 * 5"></span>
</div>-->
<!--        <div ng-app="" ng-init="mycol='lightblue'">
            <input type="text" value="{{mycol}}" ng-model="mycol" style="background-color: {{mycol}}" />           
        </div>-->
<!--AngularJS Expressions End-->


<!--AngularJS Numbers start-->
<!--        <div ng-app="" ng-init="qu='2';cost='30'">
            <span ng-bind="qu*cost"></span>
        </div>-->
<!--        <div ng-app="" ng-init="qu='2';cost='30'">
            {{qu * cost}}
        </div>-->
<!--AngularJS Numbers End-->



<!--AngularJS Strings Start-->
<!--<div ng-app="" ng-init="firstName='Rajesh';lastname='Palande'">
    {{firstName + " " + lastname}}
</div>-->
<!--<div ng-app="" ng-init="FirstName='Rajesh';LastName='Palande'">
    <span ng-bind="FirstName + ' ' + LastName"></span>
</div>-->
<!--AngularJS Strings End-->



<!--AngularJS Objects Start-->
<!--<div ng-app="" ng-init="person={firstName:'Rajesh',lastName:'Palande'}">
    {{person.firstName}}
</div>-->
<!--<div ng-app="" ng-init="person={firstName:'Raj',lastName:'Dalvi'}">
    <span ng-bind="person.lastName"></span>
</div>-->
<!--AngularJS Objects End-->


<!--AngularJS Arrays start-->
<!--<div ng-app="" ng-init="point=[1,2,5,7]">
    {{ point[3] }}
</div>-->
<div ng-app="" ng-init="point=[1,2,7,33]">
    <span ng-bind="point[3]"></span>
</div>
<!--AngularJS Arrays end-->
    </body>
</html>