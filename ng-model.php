<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <body>
        <div ng-app="myApp" ng-controller="myContro">
            Name : <input ng-model="name"  />
        </div>
        
        <script>
            var app = angular.module("myApp",[]);
            app.controller('myContro',function ($scope){
                $scope.name = 'rajesh';
            });
        </script>
    </body>
</html>