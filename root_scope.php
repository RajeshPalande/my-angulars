<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    </head>
    
    <body ng-app="myApp">
        <h1>{{color}}</h1>
        <hr/>
        <h1 ng-controller="myColor">{{color}}</h1>
        <hr/>
        <h1>{{color}}</h1>
        
        <script>
            var app = angular.module("myApp",[]);
            app.run(function($rootScope){
                $rootScope.color = "Yellow";
            });
            app.controller('myColor', function($scope) {
                $scope.color = "red";
            });
        </script>
    </body>
</html>